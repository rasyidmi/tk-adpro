package com.advanceprogramming.tasktracker.service;
import com.advanceprogramming.tasktracker.dto.MahasiswaDto;
import com.advanceprogramming.tasktracker.model.Logs;
import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.repository.LogsRepository;
import com.advanceprogramming.tasktracker.repository.MahasiswaRepository;
import com.advanceprogramming.tasktracker.repository.MataKuliahRepository;
import com.advanceprogramming.tasktracker.repository.TugasRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private TugasRepository tugasRepository;

    @Mock
    private LogsRepository logsRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @Mock
    private MataKuliahServiceImpl mataKuliahService;


    private Mahasiswa mahasiswa;
    private Mahasiswa mahasiswa2;
    private MahasiswaDto mahasiswaDto;
    private Mahasiswa admin;
    private MataKuliah matkul;
    private Tugas tugas;
    private Logs logs;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906280000", "tomwijaya", "123", "Tom Wijaya", "MAHASISWA");
        mahasiswa2 = new Mahasiswa();
        mahasiswaDto = new MahasiswaDto("1906285000", "tomwijaya", "Tom Wijaya", "123");
        mahasiswa.setMataKuliah(new HashSet<>());
        admin = new Mahasiswa("1906290000", "benq", "321", "Benjamin Qi", "ADMIN");
        matkul = new MataKuliah("Advanced Programming", 3);
        matkul.setMahasiswa(new HashSet<>());
        matkul.setListTugas(new HashSet<>());
        LocalDateTime deadline = LocalDateTime.parse("2020-07-23T20:22");
        mahasiswa.setListTugas(new HashSet<>());
        tugas = new Tugas("Sprint1", "Tugas Kelompok", deadline);
        tugas.setMataKuliah(matkul);
        mahasiswa.setLogs(new HashSet<>());
        logs = new Logs("add", mahasiswa);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906280000")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    void testServiceCreateMahasiswa() {
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.createMahasiswa(mahasiswa);
        Assertions.assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }


    @Test
    void testServiceLoadByUsernameNotFound() {
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
        boolean ret = false;
        try{
            lenient().when(mahasiswaService.loadUserByUsername("1906285560")).thenReturn(mahasiswa);
            mahasiswaService.loadUserByUsername("1906285560");
        }
        catch (Exception e){
            ret = true;
        }
        Assertions.assertTrue(ret);
    }


//    @Test
//    void testServiceCreateMahasiswaExist() {
//        mahasiswaService.createMahasiswa(mahasiswa);
//        reset(mahasiswaRepository);
//        Mahasiswa resultMahasiswa = mahasiswaService.createMahasiswa(mahasiswa);
//        Assertions.assertEquals(null, resultMahasiswa);
//    }

    @Test
    void testServiceDeleteMahasiswa() {
        mahasiswaService.createMahasiswa(mahasiswa);
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        reset(mahasiswaRepository);
        Assertions.assertEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    void testServiceUpdateMahasiswa() {
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswaService.updateRoleMahasiswa("ADMIN", mahasiswa, admin);
        Assertions.assertTrue(mahasiswa.getRole().equalsIgnoreCase("ADMIN"));
    }

    @Test
    void testServiceUpdateMahasiswaNotByAdmin() {
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswaService.updateRoleMahasiswa("ADMIN", mahasiswa, mahasiswa);
        Assertions.assertTrue(mahasiswa.getRole().equalsIgnoreCase("MAHASISWA"));
    }

    @Test
    void testServiceSubscribeMataKuliahMahasiswa() {
        Set<Tugas> listTugas = new HashSet<>();
        listTugas.add(tugas);
        matkul.setListTugas(listTugas);

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahRepository.findById(matkul.getId())).thenReturn(matkul);
        when(mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), matkul.getId())).thenReturn(mahasiswa);

        mahasiswa = mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), matkul.getId());

        Assertions.assertFalse(mahasiswa.getMataKuliah().isEmpty());

    }

    @Test
    void testServiceUnsubscribeMataKuliahMahasiswa() {
        Set<Tugas> listTugas = new HashSet<>();
        listTugas.add(tugas);
        matkul.setListTugas(listTugas);

        mahasiswa.getMataKuliah().add(matkul);
        matkul.getMahasiswa().add(mahasiswa);

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahRepository.findById(matkul.getId())).thenReturn(matkul);
        lenient().when(mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), matkul.getId())).thenReturn(mahasiswa);
        lenient().when(mahasiswaService.unsubscribeMataKuliah(mahasiswa.getNpm(), matkul.getId())).thenReturn(mahasiswa);

        mahasiswa = mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), matkul.getId());
        mahasiswa = mahasiswaService.unsubscribeMataKuliah(mahasiswa.getNpm(), matkul.getId());
        Assertions.assertTrue(mahasiswa.getMataKuliah().isEmpty());

    }

    @Test
    public void testServiceGetListTugas(){
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahRepository.findById(matkul.getId())).thenReturn(matkul);
        when(mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), matkul.getId())).thenReturn(mahasiswa);

        mahasiswa = mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), matkul.getId());
        Iterable<Tugas> listTugas = tugasRepository.findAll();
        lenient().when(mahasiswaService.getListTugasByNpm(mahasiswa.getNpm())).thenReturn(listTugas);
        Iterable<Tugas> listTugasResult = mahasiswaService.getListTugasByNpm(mahasiswa.getNpm());
        Assertions.assertIterableEquals(listTugas, listTugasResult);
    }

    @Test
    public void testServiceCompleteTugasByNpm(){
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(tugasRepository.findById(tugas.getId())).thenReturn(tugas);
        mahasiswa.addTugas(tugas);
        mahasiswaService.completeTugasByNpm(mahasiswa.getNpm(), tugas.getId());
        Iterable<Tugas> listTugasResult = mahasiswaService.getListTugasByNpm(mahasiswa.getNpm());
        int cnt = 0;
        for(Tugas tugas : listTugasResult){
            cnt++;
        }
        Assertions.assertEquals(cnt, 0);
    }

    @Test
    public void testServiceGetListLogs(){
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.addLogs(logs);
        Iterable<Logs> listLogsResult = mahasiswaService.getListLogsByNpm(mahasiswa.getNpm());
        int cnt = 0;
        for(Logs logs : listLogsResult){
            cnt++;
        }
        Assertions.assertEquals(cnt, 1);
    }

    @Test
    public void testServiceDeleteLogs(){
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logsRepository.findById(logs.getId())).thenReturn(logs);
        mahasiswa.addLogs(logs);
        mahasiswaService.deleteLogs(mahasiswa.getNpm(), logs.getId());
        Iterable<Logs> listLogsResult = mahasiswaService.getListLogsByNpm(mahasiswa.getNpm());
        int cnt = 0;
        for(Logs logs : listLogsResult){
            cnt++;
        }
        Assertions.assertEquals(cnt, 0);
    }



    @Test
    void testServiceCounter() {
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mahasiswa.incCounter();
        mahasiswaService.resetCounter(mahasiswa.getNpm());
        Assertions.assertEquals(0, mahasiswaService.getCounter(mahasiswa.getNpm()));
    }

    @Test
    void testModelMahasiswa() {
        Assertions.assertEquals("Tom Wijaya", mahasiswa.getNama());
        Assertions.assertEquals("tomwijaya", mahasiswa.getUsername());
        Assertions.assertEquals("123", mahasiswa.getPassword());
        mahasiswa.setNama("Tomy Wijayanto");
        mahasiswa.setUsername("tomwi");
        mahasiswa.setNpm("1906280000");
        mahasiswa.setPassword("321");
        mahasiswa.addMataKuliah(matkul);
        mahasiswa.addLogs(logs);
        mahasiswa.addTugas(tugas);
        Assertions.assertEquals("Tomy Wijayanto", mahasiswa.getNama());
        Assertions.assertEquals("tomwi", mahasiswa.getUsername());
        Assertions.assertEquals("1906280000", mahasiswa.getNpm());
        Assertions.assertEquals("321", mahasiswa.getPassword());
        Assertions.assertTrue(mahasiswa.isAccountNonExpired());
        Assertions.assertTrue(mahasiswa.isAccountNonLocked());
        Assertions.assertTrue(mahasiswa.isCredentialsNonExpired());
        Assertions.assertTrue(mahasiswa.isEnabled());
        Assertions.assertNotNull(mahasiswa.getAuthorities());
        Set<MataKuliah> listMatkul = new HashSet<MataKuliah>();
        Set<Tugas> listTugas = new HashSet<Tugas>();
        Set<Logs> listLogs = new HashSet<Logs>();
        listMatkul.add(matkul);
        listTugas.add(tugas);
        listLogs.add(logs);
        Assertions.assertEquals(mahasiswa.getMataKuliah(), listMatkul);
        Assertions.assertEquals(mahasiswa.getListTugas(), listTugas);
        Assertions.assertEquals(mahasiswa.getLogs(), listLogs);
        mahasiswa.removeTugas(tugas);
        mahasiswa.removeMataKuliah(matkul);
        listTugas.remove(tugas);
        listMatkul.remove(matkul);
        Assertions.assertEquals(mahasiswa.getMataKuliah(), listMatkul);
        Assertions.assertEquals(mahasiswa.getListTugas(), listTugas);
        Assertions.assertNotNull(logs.getTimeFormatted());
    }

    @Test
    void testModelMahasiswaDto() {
        Assertions.assertEquals("Tom Wijaya", mahasiswaDto.getNama());
        Assertions.assertEquals("1906285000", mahasiswaDto.getNPM());
        Assertions.assertEquals("tomwijaya", mahasiswaDto.getUsername());
        Assertions.assertEquals("123", mahasiswaDto.getPassword());
        mahasiswaDto.setNama("Tomy Wijayanto");
        mahasiswaDto.setUsername("tomwi");
        mahasiswaDto.setNPM("1906290000");
        mahasiswaDto.setPassword("321");
        Assertions.assertEquals("Tomy Wijayanto", mahasiswaDto.getNama());
        Assertions.assertEquals("tomwi", mahasiswaDto.getUsername());
        Assertions.assertEquals("1906290000", mahasiswaDto.getNPM());
        Assertions.assertEquals("321", mahasiswaDto.getPassword());
    }
}
