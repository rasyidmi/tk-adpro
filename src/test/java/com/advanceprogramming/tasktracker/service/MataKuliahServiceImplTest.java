package com.advanceprogramming.tasktracker.service;
import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.repository.MahasiswaRepository;
import com.advanceprogramming.tasktracker.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.reset;

@ExtendWith(MockitoExtension.class)
public class MataKuliahServiceImplTest {
    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private MataKuliahServiceImpl mataKuliahService;

    private MataKuliah matkul;
    private Tugas tugas;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah("Advanced Programming", 3);
        LocalDateTime deadline = LocalDateTime.parse("2020-07-23T20:22");
        tugas = new Tugas("Sprint1", "Tugas Kelompok", deadline);
        mahasiswa = new Mahasiswa("1906280000", "tomwijaya", "123", "Tom Wijaya", "MAHASISWA");
        matkul.setListTugas(new HashSet<>());
        matkul.setMahasiswa(new HashSet<>());
    }

    @Test
    void testServiceGetListMataKuliah() {
        Iterable<MataKuliah> listMataKuliah = mataKuliahRepository.findAll();
        lenient().when(mataKuliahService.getListMataKuliah()).thenReturn(listMataKuliah);
        Iterable<MataKuliah> listMataKuliahResult = mataKuliahService.getListMataKuliah();
        Assertions.assertIterableEquals(listMataKuliah, listMataKuliahResult);
    }

    @Test
    void testServiceGetMataKuliah() {
        lenient().when(mataKuliahService.getMataKuliahById(matkul.getId())).thenReturn(matkul);
        MataKuliah resultMatkul = mataKuliahService.getMataKuliahById(matkul.getId());
        Assertions.assertEquals(matkul.getId(), resultMatkul.getId());
    }

    @Test
    void testServiceGetMataKuliahByNpm() {
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahService.getMataKuliahById(matkul.getId())).thenReturn(matkul);
        Iterable<MataKuliah> resultMatkul = mataKuliahService.getListMataKuliahByNPM(mahasiswa.getNpm());
        Assertions.assertNotNull(resultMatkul);
    }

    @Test
    void testServiceGetNonMataKuliahByNpm() {
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahService.getMataKuliahById(matkul.getId())).thenReturn(matkul);
        Iterable<MataKuliah> resultMatkul = mataKuliahService.getListNonMataKuliahByNPM(mahasiswa.getNpm());
        Assertions.assertNotNull(resultMatkul);
    }

    @Test
    void testServiceCreateMataKuliah() {
        lenient().when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
        MataKuliah resultMatkul = mataKuliahService.createMataKuliah(matkul);
        Assertions.assertEquals(matkul.getId(), resultMatkul.getId());
    }

    @Test
    void testServiceDeleteMataKuliah() {
        mataKuliahService.createMataKuliah(matkul);
        lenient().when(mataKuliahRepository.findById(matkul.getId())).thenReturn(matkul);
        mataKuliahService.deleteMataKuliahById(matkul.getId());
        reset(mataKuliahRepository);
        Assertions.assertEquals(null, mataKuliahService.getMataKuliahById(matkul.getId()));
    }

    @Test
    void testModelMataKuliah() {
        Assertions.assertEquals("Advanced Programming", matkul.getNama());
        Assertions.assertEquals(3, matkul.getSks());
        matkul.setId(1);
        matkul.setNama("Projut");
        matkul.setSks(4);
        Assertions.assertEquals("Projut", matkul.getNama());
        Assertions.assertEquals(4, matkul.getSks());
        Set<Mahasiswa> listMahasiswa = new HashSet<>();
        Set<Tugas> listTugas = new HashSet<>();
        listMahasiswa.add(mahasiswa);
        listTugas.add(tugas);
        matkul.addMahasiswa(mahasiswa);
        matkul.addTugas(tugas);
        Assertions.assertEquals(listMahasiswa, matkul.getMahasiswa());
        Assertions.assertEquals(listTugas, matkul.getListTugas());
        listMahasiswa.remove(mahasiswa);
        listTugas.remove(tugas);
        matkul.removeMahasiswa(mahasiswa);
        matkul.removeTugas(tugas);
        Assertions.assertEquals(listMahasiswa, matkul.getMahasiswa());
        Assertions.assertEquals(listTugas, matkul.getListTugas());
    }
}
