package com.advanceprogramming.tasktracker.service;
import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.repository.MataKuliahRepository;
import com.advanceprogramming.tasktracker.repository.TugasRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TugasServiceImplTest {
    @Mock
    private TugasRepository tugasRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private TugasServiceImpl tugasService;

    private Tugas tugas;
    private MataKuliah matkul;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        LocalDateTime deadline = LocalDateTime.parse("2020-07-23T20:22");
        tugas = new Tugas("Sprint1", "Tugas Kelompok", deadline);
        matkul = new MataKuliah("Advanced Programming", 3);
        matkul.setListTugas(new HashSet<>());
        matkul.setMahasiswa(new HashSet<>());
        tugas.setMataKuliah(matkul);
        mahasiswa = new Mahasiswa("1906280000", "tomwijaya", "123", "Tom Wijaya", "MAHASISWA");
    }

    @Test
    void testServiceGetListTugas() {
        Iterable<Tugas> listTugas = tugasRepository.findByMataKuliah(matkul);
        lenient().when(tugasService.getListTugasByIdMatkul(matkul.getId())).thenReturn(listTugas);
        Iterable<Tugas> listTugasResult = tugasService.getListTugasByIdMatkul(matkul.getId());
        Assertions.assertIterableEquals(listTugas, listTugasResult);
    }

    @Test
    void testServiceGetTugas() {
        lenient().when(tugasService.getTugasById(tugas.getId())).thenReturn(tugas);
        Tugas resultTugas = tugasService.getTugasById(tugas.getId());
        Assertions.assertEquals(matkul.getId(), resultTugas.getId());
    }

    @Test
    void testServiceCreateTugas() {
        when(mataKuliahRepository.findById(matkul.getId())).thenReturn(matkul);
//        when(mataKuliahRepository.findById(matkul.getId())).thenReturn(matkul);
        lenient().when(tugasService.createTugas(tugas, matkul.getId())).thenReturn(tugas);
        Tugas resultTugas = tugasService.createTugas(tugas, matkul.getId());
        Assertions.assertEquals(tugas.getId(), resultTugas.getId());
    }

    @Test
    void testServiceDeleteTugas() {
        lenient().when(tugasRepository.findById(tugas.getId())).thenReturn(tugas);
        tugasService.deleteTugasById(tugas.getId());
        reset(tugasRepository);
        Assertions.assertEquals(0, tugasRepository.findAll().size());
    }

    @Test
    void testModelTugas() {
        Assertions.assertEquals("Sprint1", tugas.getJudul());
        Assertions.assertEquals("Tugas Kelompok", tugas.getDeskripsi());
        Assertions.assertEquals(tugas.getDeadline(), LocalDateTime.parse("2020-07-23T20:22"));
        Assertions.assertEquals(tugas.getdeadline(), LocalDateTime.parse("2020-07-23T20:22"));
        tugas.setId(1);
        tugas.setJudul("Sprint2");
        tugas.setDeskripsi("TK");
        tugas.setDeadline(LocalDateTime.parse("2020-07-23T20:23"));
        Assertions.assertEquals("Sprint2", tugas.getJudul());
        Assertions.assertEquals("TK", tugas.getDeskripsi());
        Assertions.assertEquals(tugas.getDeadlineFormatted(), "2020-07-23 20:23");
        Assertions.assertEquals(matkul, tugas.getMataKuliah());
        tugas.setListMahasiswa(new HashSet<>());
        Set<Mahasiswa> listMahasiswa = new HashSet<>();
        listMahasiswa.add(mahasiswa);
        listMahasiswa.remove(mahasiswa);
        tugas.addMahasiswa(mahasiswa);
        tugas.removeMahasiswa(mahasiswa);
        Assertions.assertEquals(listMahasiswa, tugas.getListMahasiswa());
    }
}
