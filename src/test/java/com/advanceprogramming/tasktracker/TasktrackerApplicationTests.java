package com.advanceprogramming.tasktracker;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TasktrackerApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void testMain() {
		TasktrackerApplication.main(new String[] {});
	}


}
