package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = RegistrationController.class)
public class RegistrationControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @Test
    void testOpenRegisterPage() throws Exception {
        mvc.perform(get("/registration"))
                .andExpect(status().isOk());
    }


}
