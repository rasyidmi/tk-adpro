package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import com.advanceprogramming.tasktracker.service.MataKuliahServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = MataKuliahController.class)
class MataKuliahControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MataKuliah matkul;
    private Mahasiswa admin;


    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah("Advanced Programming", 3);
        admin = new Mahasiswa("1906291233", "admin", "admin", "admin", "admin");
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    void testControllerCreateMataKuliah() throws Exception {
        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
        mvc.perform(post("/matakuliah")).andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    void testControllerDeleteMataKuliah() throws Exception {
        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
        mvc.perform(post("/matakuliah/" + matkul.getId())).andExpect(status().is3xxRedirection());
    }


//    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerListMataKuliah() throws Exception {
//        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
//        mvc.perform(get("/matakuliah")).andExpect(status().isOk());
//    }
//
//    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerListDeleteMataKuliah() throws Exception {
//        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
//        mvc.perform(get("/matakuliah/deleteMatkul")).andExpect(status().isOk());
//    }


}
