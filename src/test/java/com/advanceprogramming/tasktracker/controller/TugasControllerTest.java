package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import com.advanceprogramming.tasktracker.service.MataKuliahServiceImpl;
import com.advanceprogramming.tasktracker.service.TugasServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = TugasController.class)
class TugasControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TugasServiceImpl tugasService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @Autowired
    private WebApplicationContext webApplicationContext;


    private Tugas tugas;
    private MataKuliah matkul;
    private Mahasiswa admin;

    @BeforeEach
    public void setUp() {
        admin = new Mahasiswa("1906291233", "admin", "admin", "admin", "admin");
        LocalDateTime deadline = LocalDateTime.parse("2020-07-23T20:22");
        tugas = new Tugas("Sprint1", "Tugas Kelompok", deadline);
        matkul = new MataKuliah("Advanced Programming", 3);
        tugas.setMataKuliah(matkul);
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    void testControllerDeleteTugas() throws Exception {
        tugasService.createTugas(tugas, matkul.getId());
        mvc.perform(post("/tugas/deleteTugas/" + matkul.getId()+ "/" + tugas.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection());
    }

    //    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerGetListTugas() throws Exception {
//        Iterable<Tugas> listTugas = Arrays.asList(tugas);
//        when(tugasService.getListTugasByIdMatkul(matkul.getId())).thenReturn(listTugas);
//
//        mvc.perform(get("/tugas/" + matkul.getId()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].id").value(tugas.getId()));
//    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerCreateTugas() throws Exception {
//        tugasService.createTugas(tugas, matkul.getId());
//
//        mvc.perform(post("/tugas/addTugas/" + matkul.getId()))
//                .andExpect(status().isOk());
//    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerGetTugasByMatkul() throws Exception {
//        when(tugasService.getTugasById(tugas.getId())).thenReturn(tugas);
//        mvc.perform(get("/tugas/" +  matkul.getId()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andReturn();
//    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerGetTugas() throws Exception {
//        when(tugasService.getTugasById(tugas.getId())).thenReturn(tugas);
//        mvc.perform(get("/tugas/" +  matkul.getId() + "/" + tugas.getId()).contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andReturn();
//    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
//    void testControllerGetTugasNotFound() throws Exception {
//        mvc.perform(get("/tugas/" + matkul.getId() + "/123").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//    }

}
