package com.advanceprogramming.tasktracker.controller;


import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;

import com.advanceprogramming.tasktracker.service.MataKuliahService;
import com.advanceprogramming.tasktracker.service.MataKuliahServiceImpl;
import com.advanceprogramming.tasktracker.service.TugasService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MahasiswaController.class)
public class MahasiswaControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MataKuliah mataKuliah;

    private Mahasiswa mahasiswa;

    private Mahasiswa admin;

    private Tugas tugas;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906291234", "ayam", "ayamgoreng", "Rembo", "admin");
//        admin = new Mahasiswa("1906291233", "admin", "admin", "admin", "admin");
        mataKuliah = new MataKuliah("Adpro", 4);
        LocalDateTime deadline = LocalDateTime.parse("2020-07-23T20:22");
        tugas = new Tugas("Sprint1", "Tugas Kelompok", deadline);
        tugas.setMataKuliah(mataKuliah);
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    public void testControllerGetNonExistMahasiswa() throws Exception{
        mvc.perform(get("/mahasiswa/1906294321").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    void testControllerSubscribeMataKuliah() throws Exception {
        mahasiswaService.createMahasiswa(mahasiswa);
        mataKuliahService.createMataKuliah(mataKuliah);

        when(mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), mataKuliah.getId())).thenReturn(mahasiswa);

        mvc.perform(post("/mahasiswa/subscribe/" + mahasiswa.getNpm())
                .param("idMatakuliah", Integer.toString(mataKuliah.getId())))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    void testControllerUnsubscribeMataKuliah() throws Exception {
        mahasiswaService.createMahasiswa(mahasiswa);
        mataKuliahService.createMataKuliah(mataKuliah);
        mahasiswaService.subscribeMataKuliah(mahasiswa.getNpm(), mataKuliah.getId());

        when(mahasiswaService.unsubscribeMataKuliah(mahasiswa.getNpm(), mataKuliah.getId())).thenReturn(mahasiswa);

        mvc.perform(post("/mahasiswa/unsubscribe/" + mahasiswa.getNpm())
                .param("idMatakuliah", Integer.toString(mataKuliah.getId())))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void testControllerSubscribeMataKuliahForbidden() throws Exception {
        mvc.perform(post("/subscribe/{npm}?idMatakuliah=0", "1906291234"))
                .andExpect(status().is4xxClientError());

    }

}
