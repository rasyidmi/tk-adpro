package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/mahasiswa")
public class MahasiswaController {

    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    //Subscribe matkul
    @PostMapping(path = "/subscribe/{npm}")
    public String subscribeMataKuliah(@PathVariable(value = "npm") String npm, @RequestParam int idMatakuliah) {
        mahasiswaService.subscribeMataKuliah(npm, idMatakuliah);
        return "redirect:/matakuliah";
    }

    //Unsub matkul
    @PostMapping(path = "/unsubscribe/{npm}")
    public String unsubscribeMataKuliah(@PathVariable(value = "npm") String npm, @RequestParam int idMatakuliah) {
        mahasiswaService.unsubscribeMataKuliah(npm, idMatakuliah);
        return "redirect:/matakuliah";
    }

    //Complete salah satu matkul
    @PostMapping(path = "/completeTugas/{idTugas}")
    public String completeTugas(@PathVariable(value = "idTugas") int idTugas) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            mahasiswaService.completeTugasByNpm(mahasiswa.getNpm(), idTugas);
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/home?error";
        }

        return "redirect:/home?success";
    }

    //Menghapus log
    @PostMapping(path = "/deleteLogs/{idLogs}")
    public String deleteLogs(@PathVariable(value = "idLogs") int idLogs) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            mahasiswaService.deleteLogs(mahasiswa.getNpm(), idLogs);
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/notification?error";
        }

        return "redirect:/notification?success";
    }

}
