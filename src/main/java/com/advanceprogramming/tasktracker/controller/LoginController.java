package com.advanceprogramming.tasktracker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/login")
public class LoginController {

    //Halaman login
    @GetMapping
    public String page(){
        return "loginpage";
    }

}
