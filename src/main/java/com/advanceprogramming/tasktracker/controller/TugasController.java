package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import com.advanceprogramming.tasktracker.service.TugasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(path = "/tugas")
public class TugasController {

    @Autowired
    private TugasService tugasService;

    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    //Menampilkan halaman yang menampilkan form data untuk penambahan tugas sesuai
    //dengan path variabel id mata kuliah.
    //Khusus admin.
    @GetMapping(path = "/addTugas/{idMatkul}")
    public String addTugasPage(@PathVariable(value = "idMatkul") int idMatkul, Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("counter", mahasiswa.getCounter());
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "addTugas";
    }

    //Menampilkan tugas-tugas yang bisa dihapus sesuai mata kuliah.
    @GetMapping(path = "/deleteTugas/{idMatkul}")
    public String deleteTugasPage(@PathVariable(value = "idMatkul") int idMatkul, Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        model.addAttribute("listTugas", tugasService.getListTugasByIdMatkul(idMatkul));
        model.addAttribute("idMatkul", idMatkul);
        return "deleteTugas";
    }

    //Menambahkan tugas pada mata kuliah.
    @PostMapping(path = "/addTugas/{idMatkul}")
    public String createTugas(@PathVariable(value = "idMatkul") int idMatkul, HttpServletRequest request) {
        String judul = request.getParameter("judul");
        String deskripsi = request.getParameter("deskripsi");
        LocalDateTime deadline = LocalDateTime.parse(request.getParameter("deadline"));

        Tugas tugas = new Tugas(judul, deskripsi, deadline);
        try {
            tugasService.createTugas(tugas, idMatkul);
            tugasService.broadcast(idMatkul, tugas.getId(), "add");
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/tugas/addTugas/{idMatkul}?error";
        }

        return "redirect:/tugas/addTugas/{idMatkul}?success";
    }

    //Menghapus tugas
    @PostMapping(path = "/deleteTugas/{idMatkul}/{idTugas}")
    public String deleteTugasById(@PathVariable(value = "idMatkul") int idMatkul, @PathVariable(value = "idTugas") int idTugas) {
        try {
            tugasService.deleteTugasById(idTugas);
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/tugas/deleteTugas/{idMatkul}";
        }

        return "redirect:/tugas/deleteTugas/{idMatkul}?success";
    }
}
//    @DeleteMapping(path = "/{idMatkul}/{id}", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity deleteTugasById(@PathVariable(value="idMatkul") int idMatkul, @PathVariable(value="id") int id) {
//        tugasService.broadcast(id, idMatkul, "del");
//        tugasService.deleteTugasById(id);
//        return new ResponseEntity(HttpStatus.NO_CONTENT);
//    }

//    @GetMapping(path = "/{idMatkul}/{id}", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity getTugasById(@PathVariable(value="idMatkul") int idMatkul, @PathVariable(value = "id") int id) {
//        Tugas tugas = tugasService.getTugasById(id);
//        if (tugas == null) {
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
//        }
//        return ResponseEntity.ok(tugas);
//    }

//    @GetMapping(path = "/{idMatkul}", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity<Iterable<Tugas>> getListTugasByIdMatkul(@PathVariable(value = "idMatkul") int idMatkul) {
//        return ResponseEntity.ok(tugasService.getListTugasByIdMatkul(idMatkul));
//    }
