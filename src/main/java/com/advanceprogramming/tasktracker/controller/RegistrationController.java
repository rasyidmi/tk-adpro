package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.dto.MahasiswaDto;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    @ModelAttribute("user")
    public MahasiswaDto mahasiswaDto() {
        return new MahasiswaDto();
    }

    //Halaman register
    @GetMapping
    public String page() {
        return "registerpage";
    }

    //API post register
    @Timed("register")
    @PostMapping
    public String register(@ModelAttribute("user") MahasiswaDto mahasiswaDto) {

        try {
            mahasiswaService.createMahasiswa(mahasiswaDto);
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/registration?error";
        }

        return "redirect:/login";
    }
}

