package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.model.Logs;
import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import com.advanceprogramming.tasktracker.service.TugasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomePageController {

    @Autowired
    TugasService tugasService;

    @Autowired
    MahasiswaServiceImpl mahasiswaService;

    @RequestMapping("")
    public String index(Model model) {
        return "redirect:/home";
    }

    //Menampilkan halaman yang berisi list deadline.
    @RequestMapping("/home")
    public String home(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Iterable<Tugas> listTugas =  tugasService.getListTugasByNPM(mahasiswa.getNpm());
        model.addAttribute("listTugas", listTugas);
        model.addAttribute("npm", mahasiswa.getNpm());
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "listDeadline";
    }

    //Halaman admin
    @GetMapping("/admin")
    public String adminPage(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "admin";
    }

    //Menampilkan halaman yang berisi list notification.
    @RequestMapping("/notification")
    public String notification(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        mahasiswaService.resetCounter(mahasiswa.getNpm());
        Iterable<Logs> listLogs =  mahasiswaService.getListLogsByNpm(mahasiswa.getNpm());
        model.addAttribute("listLogs", listLogs);
        model.addAttribute("npm", mahasiswa.getNpm());
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "notification";
    }

}

