package com.advanceprogramming.tasktracker.controller;

import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.service.MahasiswaServiceImpl;
import com.advanceprogramming.tasktracker.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

import static java.lang.Integer.parseInt;

@Controller
@RequestMapping(path = "/matakuliah")
public class MataKuliahController {

    @Autowired
    private MataKuliahService mataKuliahService;

    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    //Menampilkan halaman mata kuliah apa saja yang kita subscribe dan yang tidak
    @GetMapping
    public String listMatkulPage(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Set<MataKuliah> listMatkul =  mataKuliahService.getListMataKuliahByNPM(mahasiswa.getNpm());
        Set<MataKuliah> listBukanMatkul = mataKuliahService.getListNonMataKuliahByNPM(mahasiswa.getNpm());

        model.addAttribute("npm", mahasiswa.getNpm());
        model.addAttribute("listMatkul", listMatkul);
        model.addAttribute("listBukanMatkul", listBukanMatkul);
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));

        return "listMataKuliah";
    }

    //Halaman khusus admin di mana ditampilkan seluruh mata kuliah dan button untuk menghapusnya
    @GetMapping("/deleteMatkul")
    public String deleteMatkulPage(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Iterable<MataKuliah> listMatkul = mataKuliahService.getListMataKuliah();
        model.addAttribute("listMatkul", listMatkul);
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "deleteMataKuliah";
    }

    //Halaman khusus untuk admin di mana bisa menambahkan mata kuliah.
    @GetMapping("/addMatkul")
    public String addMatkulPage(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "addMataKuliah";
    }

    //Post request untuk menambahkan mata kuliah.
    @PostMapping
    public String createMataKuliah(@ModelAttribute("matkul") MataKuliah mataKuliah) {
        try {
            mataKuliahService.createMataKuliah(mataKuliah);
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/matakuliah/addMatkul?error";
        }

        return "redirect:/matakuliah/addMatkul?success";
    }

    //Halaman khusus admin yang menampilkan seluruh mata kuliah yang ada dan button untuk menambah/menghapus tugas
    //untuk setiap mata kuliah.
    @GetMapping("/editTugas")
    public String editTugasListPage(Model model) {
        Mahasiswa mahasiswa = (Mahasiswa) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Iterable<MataKuliah> listMatkul =  mataKuliahService.getListMataKuliah();
        model.addAttribute("listMatkul", listMatkul);
        model.addAttribute("counter", mahasiswaService.getCounter(mahasiswa.getNpm()));
        return "editTugasListPage";
    }

    //Menghapus mata kuliah
    @PostMapping("/{id}")
    public String deleteMataKuliah(@PathVariable(value="id") int id) {
        try {
            mataKuliahService.deleteMataKuliahById(id);
        }

        catch (Exception DuplicateKeyError) {
            return "redirect:/matakuliah/deleteMatkul?error";
        }

        return "redirect:/matakuliah/deleteMatkul?success";
    }

    //API mendapatkan mata kuliah tertentu dengan ID
    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMataKuliahById(@PathVariable(value = "id") int id) {
        MataKuliah mataKuliah = mataKuliahService.getMataKuliahById(id);
        if (mataKuliah == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(mataKuliah);
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<MataKuliah>> getListMataKuliah() {
        return ResponseEntity.ok(mataKuliahService.getListMataKuliah());
    }
}
