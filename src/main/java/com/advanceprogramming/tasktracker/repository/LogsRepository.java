package com.advanceprogramming.tasktracker.repository;

import com.advanceprogramming.tasktracker.model.Logs;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogsRepository extends JpaRepository<Logs, Integer>{
    Logs findById(int id);
}
