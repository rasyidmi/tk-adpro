package com.advanceprogramming.tasktracker.repository;

import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.sun.istack.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TugasRepository extends JpaRepository<Tugas, Integer>{
    Tugas findById(int id);

    List<Tugas> findByMataKuliah(MataKuliah mataKuliah);
}
