package com.advanceprogramming.tasktracker.repository;

import com.advanceprogramming.tasktracker.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa, String> {
    Mahasiswa findByNpm(String npm);
}
