package com.advanceprogramming.tasktracker.repository;

import com.advanceprogramming.tasktracker.model.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MataKuliahRepository extends JpaRepository<MataKuliah, Integer>{
    MataKuliah findById(int id);
}
