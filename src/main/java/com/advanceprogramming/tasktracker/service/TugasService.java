package com.advanceprogramming.tasktracker.service;

import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;

import java.util.Set;

public interface TugasService {
    Tugas createTugas(Tugas tugas, int idMatkul);

    void deleteTugasById(int id);

    Iterable<Tugas> getListTugas();

    Iterable<Tugas> getListTugasByIdMatkul(int id);

    Tugas getTugasById(int id);

    Set<Tugas> getListTugasByNPM(String npm);

    void broadcast(int idMatkul, int idTugas, String pesan);
}
