package com.advanceprogramming.tasktracker.service;


import com.advanceprogramming.tasktracker.model.Logs;

import com.advanceprogramming.tasktracker.dto.MahasiswaDto;
import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.repository.LogsRepository;
import com.advanceprogramming.tasktracker.repository.MahasiswaRepository;
import com.advanceprogramming.tasktracker.repository.MataKuliahRepository;
import com.advanceprogramming.tasktracker.repository.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class MahasiswaServiceImpl implements UserDetailsService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private TugasRepository tugasRepository;

    @Autowired
    private LogsRepository logsRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public Mahasiswa createMahasiswa(MahasiswaDto mahasiswaDto) {
        if (mahasiswaRepository.findByNpm(mahasiswaDto.getNPM()) != null) {

            throw new DuplicateKeyException("NPM already registered!");
        }
        Mahasiswa mahasiswa = new Mahasiswa(mahasiswaDto.getNPM(), mahasiswaDto.getUsername(),
                passwordEncoder.encode(mahasiswaDto.getPassword()), mahasiswaDto.getNama(), "MAHASISWA");
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        if (mahasiswaRepository.findByNpm(mahasiswa.getNpm()) != null) {
            throw new DuplicateKeyException("NPM sudah terdaftar!");
        }

        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    public void deleteMahasiswaByNPM(String npm) {
        if (mahasiswaRepository.findByNpm(npm) != null) {
            mahasiswaRepository.deleteById(npm);
        }
    }


    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public UserDetails loadUserByUsername(String npm) throws UsernameNotFoundException {
        Mahasiswa mahasiswa = getMahasiswaByNPM(npm);
        if(mahasiswa == null) {
            throw new UsernameNotFoundException("Invalid NPM or password.");
        }
        return mahasiswa;
    }

    public Mahasiswa updateRoleMahasiswa(String newRole, Mahasiswa mahasiswa, Mahasiswa admin) {
        if (admin.getRole().equalsIgnoreCase("admin")) {
            mahasiswa.setRole(newRole);
            mahasiswaRepository.save(mahasiswa);
            return mahasiswa;
        }
        return null;

    }

    public Mahasiswa subscribeMataKuliah(String npm, int idMataKuliah) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        MataKuliah mataKuliah = mataKuliahRepository.findById(idMataKuliah);
        if (mahasiswa != null && mataKuliah != null) {
            mahasiswa.addMataKuliah(mataKuliah);
            mataKuliah.addMahasiswa(mahasiswa);
            for (Tugas tugas : mataKuliah.getListTugas()) {
                if (tugas.getMataKuliah() == mataKuliah) {
                    mahasiswa.addTugas(tugas);
                    tugas.addMahasiswa(mahasiswa);
                }
            }
            mahasiswaRepository.save(mahasiswa);
            return mahasiswa;
        }
        return null;
    }

    public Mahasiswa unsubscribeMataKuliah(String npm, int idMataKuliah) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        MataKuliah mataKuliah = mataKuliahRepository.findById(idMataKuliah);
        if (mahasiswa.getMataKuliah().contains(mataKuliah)) {
            mahasiswa.removeMataKuliah(mataKuliah);
            mataKuliah.removeMahasiswa(mahasiswa);
            for (Tugas tugas : mataKuliah.getListTugas()) {
                mahasiswa.removeTugas(tugas);
                tugas.removeMahasiswa(mahasiswa);
            }
            mahasiswaRepository.save(mahasiswa);
            return mahasiswa;
        }
        return null;
    }

    public Iterable<Tugas> getListTugasByNpm(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        List<Tugas> daftarTugas = new ArrayList<>(){};
        Set<MataKuliah> daftarMatkul = mahasiswa.getMataKuliah();
        for(MataKuliah matkul : daftarMatkul) {
            daftarTugas.addAll(tugasRepository.findByMataKuliah(matkul));
        }
        return daftarTugas;
    }

    public Iterable<Logs> getListLogsByNpm(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return mahasiswa.getLogsSorted();
    }

    public void completeTugasByNpm(String npm, int idTugas) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        Tugas tugas = tugasRepository.findById(idTugas);
        mahasiswa.removeTugas(tugas);
        tugas.removeMahasiswa(mahasiswa);

        mahasiswaRepository.save(mahasiswa);
    }

    public void deleteLogs(String npm, int idLogs) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        Logs logs = logsRepository.findById(idLogs);
        mahasiswa.removeLogs(logs);
        mahasiswaRepository.save(mahasiswa);
    }

    public void resetCounter(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        mahasiswa.resetCounter();
        mahasiswaRepository.save(mahasiswa);
    }

    public int getCounter(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa.getCounter();
    }



}
