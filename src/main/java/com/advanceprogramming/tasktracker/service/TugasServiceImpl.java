package com.advanceprogramming.tasktracker.service;

import com.advanceprogramming.tasktracker.model.Logs;
import com.advanceprogramming.tasktracker.model.Mahasiswa;
import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.model.Tugas;
import com.advanceprogramming.tasktracker.repository.LogsRepository;
import com.advanceprogramming.tasktracker.repository.MahasiswaRepository;
import com.advanceprogramming.tasktracker.repository.MataKuliahRepository;
import com.advanceprogramming.tasktracker.repository.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;


@Service
public class TugasServiceImpl implements TugasService{
    @Autowired
    private TugasRepository tugasRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private LogsRepository logsRepository;

    @Override
    public Tugas createTugas(Tugas tugas, int idMatkul) {
        MataKuliah mataKuliah = mataKuliahRepository.findById(idMatkul);
        tugas.setMataKuliah(mataKuliah);
        mataKuliah.addTugas(tugas);

        for (Mahasiswa mahasiswa : mataKuliah.getMahasiswa()) {
            mahasiswa.addTugas(tugas);
            tugas.addMahasiswa(mahasiswa);
        }

        tugasRepository.save(tugas);
        return tugas;
    }

    @Override
    public void deleteTugasById(int id) {
        Tugas tugas = tugasRepository.findById(id);
        if (tugas != null) {
            for(Mahasiswa mahasiswa : tugas.getListMahasiswa()) {
                mahasiswa.removeTugas(tugas);
            }
            tugasRepository.deleteById(id);
        }
    }

    @Override
    public Iterable<Tugas> getListTugas() {
        return tugasRepository.findAll();
    }

    @Override
    public Iterable<Tugas> getListTugasByIdMatkul(int idMatkul) {
        MataKuliah mataKuliah = mataKuliahRepository.findById(idMatkul);
        return tugasRepository.findByMataKuliah(mataKuliah);
    }

    @Override
    public Tugas getTugasById(int id) {
        return tugasRepository.findById(id);
    }

    @Override
    public Set<Tugas> getListTugasByNPM(String npm) {
        Set<Tugas> tugasResult = new HashSet<>();
        for (Tugas tugas : getListTugas()) {
            if (tugas.getListMahasiswa().contains(mahasiswaRepository.findByNpm(npm))) {
                tugasResult.add(tugas);
            }
        }
        return tugasResult;
    }

    @Override
    public void broadcast(int idMatkul, int idTugas, String tipe) {
        MataKuliah mataKuliah = mataKuliahRepository.findById(idMatkul);
        Tugas tugas = getTugasById(idTugas);
        String pesan = "Tugas " + tugas.getJudul() + " pada Mata Kuliah " + mataKuliah.getNama() + " telah ditambahkan";
        Set<Mahasiswa> mahasiswaSet = mataKuliah.getMahasiswa();
        for (Mahasiswa mhs : mahasiswaSet) {
            Logs logs = new Logs(pesan, mhs);
            mhs.incCounter();
            logsRepository.save(logs);
            mhs.addLogs(logs);
        }
    }
}
