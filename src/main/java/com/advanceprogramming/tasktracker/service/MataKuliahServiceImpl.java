package com.advanceprogramming.tasktracker.service;

import com.advanceprogramming.tasktracker.model.MataKuliah;
import com.advanceprogramming.tasktracker.repository.MahasiswaRepository;
import com.advanceprogramming.tasktracker.repository.LogsRepository;
import com.advanceprogramming.tasktracker.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MataKuliahServiceImpl implements MataKuliahService{
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private LogsRepository logRepository;

    @Override
    public MataKuliah createMataKuliah(MataKuliah mataKuliah) {
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public void deleteMataKuliahById(int id) {
        if (mataKuliahRepository.findById(id) != null) {
            mataKuliahRepository.deleteById(id);
        }
    }

    @Override
    public Iterable<MataKuliah> getListMataKuliah() {
        return mataKuliahRepository.findAll();
    }

    @Override
    public Set<MataKuliah> getListMataKuliahByNPM(String npm) {
        Set<MataKuliah> matkulResult = new HashSet<>();
        for (MataKuliah matkul : getListMataKuliah()) {
            if (matkul.getMahasiswa().contains(mahasiswaRepository.findByNpm(npm))) {
                matkulResult.add(matkul);
            }
        }
        return matkulResult;
    }

    @Override
    public Set<MataKuliah> getListNonMataKuliahByNPM(String npm) {
        Set<MataKuliah> matkulResult = new HashSet<>();
        Set<MataKuliah> matkulSub = getListMataKuliahByNPM(npm);

        for (MataKuliah matkul : getListMataKuliah()) {
            if (!matkulSub.contains(matkul)) {
                matkulResult.add(matkul);
            }
        }
        return matkulResult;
    }

    @Override
    public MataKuliah getMataKuliahById(int id) {
        return mataKuliahRepository.findById(id);
    }


}
