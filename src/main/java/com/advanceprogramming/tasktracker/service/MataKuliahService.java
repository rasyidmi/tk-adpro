package com.advanceprogramming.tasktracker.service;

import com.advanceprogramming.tasktracker.model.MataKuliah;

import java.util.Set;

public interface MataKuliahService {
    MataKuliah createMataKuliah(MataKuliah mataKuliah);

    void deleteMataKuliahById(int id);

    Iterable<MataKuliah> getListMataKuliah();

    MataKuliah getMataKuliahById(int id);

    Set<MataKuliah> getListMataKuliahByNPM(String npm);

    Set<MataKuliah> getListNonMataKuliahByNPM(String npm);
}
