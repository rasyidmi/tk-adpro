package com.advanceprogramming.tasktracker.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tugas")
@NoArgsConstructor
public class Tugas {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "judul", updatable = false, nullable = false)
    private String judul;

    @ManyToOne
    @JoinColumn(name = "id_matkul")
    private MataKuliah mataKuliah;

    @JsonIgnore
    @ManyToMany(mappedBy = "listTugas")
    private Set<Mahasiswa> listMahasiswa;

    @Column(name = "deskripsi", updatable = false, nullable = false)
    private String deskripsi;

    @Column(name = "deadline", updatable = false, nullable = false)
    private LocalDateTime deadline;



    public Tugas(String judul, String deskripsi, LocalDateTime deadline) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.deadline = deadline;
        this.listMahasiswa = new HashSet<>();
    }

    public Set<Mahasiswa> getListMahasiswa() {
        return listMahasiswa;
    }

    public int getId() {
        return id;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public String getJudul() {
        return judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getDeadlineFormatted() {
        String DATE_FORMATTER= "yyyy-MM-dd HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDateTime = deadline.format(formatter);

        return formatDateTime;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
//        this.mataKuliah.broadcast("Tugas " + tugas.getJudul() + " telah muncul");
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public LocalDateTime getdeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public void setListMahasiswa(Set<Mahasiswa> listMahasiswa) {
        this.listMahasiswa = listMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        listMahasiswa.add(mahasiswa);
    }

    public void removeMahasiswa(Mahasiswa mahasiswa) {
        listMahasiswa.remove(mahasiswa);
    }


}
