package com.advanceprogramming.tasktracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "matakuliah")
@NoArgsConstructor
public class MataKuliah  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "nama", updatable = false, nullable = false)
    private String nama;

    @Column(name = "sks", updatable = false, nullable = false)
    private int sks;

    @OneToMany(mappedBy = "mataKuliah")
    Set<Tugas> listTugas;

    @JsonIgnore
    @ManyToMany(mappedBy = "mataKuliah")
    private Set<Mahasiswa> mahasiswa;


    public MataKuliah(String nama, int sks)
    {
        this.nama = nama;
        this.sks = sks;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public int getSks() {
        return sks;
    }

    public Set<Tugas> getListTugas() {
        return listTugas;
    }

    public Set<Mahasiswa> getMahasiswa() {
        return mahasiswa;
    }

    public void setListTugas(Set<Tugas> listTugas) {
        this.listTugas = listTugas;
    }

    public void setMahasiswa(Set<Mahasiswa> mahasiswaSet) {
        this.mahasiswa = mahasiswaSet;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public void addMahasiswa(Mahasiswa mahasiswaAdd) {
        mahasiswa.add(mahasiswaAdd);
    }

    public void removeMahasiswa(Mahasiswa mahasiswaRemove) {
        mahasiswa.remove(mahasiswaRemove);
    }

    public void addTugas(Tugas tugas) {
        listTugas.add(tugas);
    }

    public void removeTugas(Tugas tugas) {
        listTugas.remove(tugas);
    }
}
