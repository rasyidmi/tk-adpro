package com.advanceprogramming.tasktracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collections;


import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "mahasiswa")
public class Mahasiswa implements UserDetails {

    @Id
    @Column(name = "npm", updatable = false, nullable = false)
    private String npm;

    @Column(name = "username", updatable = false, nullable = false)
    private String username;

    @Column(name = "password", updatable = false, nullable = false)
    private String password;

    @Column(name = "nama", updatable = false, nullable = false)
    private String nama;

    @Column(name = "role", nullable = false)
    private String role;

    @Column(name = "counter", nullable = false)
    private int counter;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Subscribe",
            joinColumns = @JoinColumn(name = "npm"),
            inverseJoinColumns = @JoinColumn(name = "id_mataKuliah"))
    Set<MataKuliah> mataKuliah;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "listTugas",
            joinColumns = @JoinColumn(name = "npm"),
            inverseJoinColumns = @JoinColumn(name = "id_tugas")
    )
    Set<Tugas> listTugas;

    @JsonIgnore
    @OneToMany(mappedBy = "mahasiswa",
            orphanRemoval = true,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    Set<Logs> logs;

    public Mahasiswa() {

    }

    public Mahasiswa(String npm, String username, String password, String nama, String role) {
        this.npm = npm;
        this.username = username;
        this.password = password;
        this.nama = nama;
        this.role = role;
        this.logs = new HashSet<>();
        this.counter = 0;
    }

    public String getNama() {
        return nama;
    }

    public String getNpm() {
        return npm;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public Set<MataKuliah> getMataKuliah() {
        return mataKuliah;
    }

    public Set<Logs> getLogs() { return logs; }

    public List<Logs> getLogsSorted() {
        List<Logs> listLogs = new ArrayList<>(logs);
        Collections.sort(listLogs, new Comparator<Logs>() {
            @Override
            public int compare(Logs p1, Logs p2) {
                return p2.getTimeFormatted().compareTo(p1.getTimeFormatted());
            }
        });
        return listLogs;
    }

    public Set<Tugas> getListTugas() { return listTugas; }

    public int getCounter(){
        return this.counter;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setMataKuliah(Set<MataKuliah> mataKuliahSet) {
        this.mataKuliah = mataKuliahSet;
    }

    public void addLogs(Logs logs) {
        this.logs.add(logs);
    }

    public void setListTugas(Set<Tugas> tugasSet) {
        this.listTugas = tugasSet;
    }

    public void setLogs(Set<Logs> logsSet) {
        this.logs = logsSet;
    }

    public void addMataKuliah(MataKuliah matkul) {
        mataKuliah.add(matkul);
    }

    public void removeMataKuliah(MataKuliah matkul) {
        mataKuliah.remove(matkul);
    }

    public void removeLogs(Logs logs) {
        this.logs.remove(logs);
    }

    public void addTugas(Tugas tugas) {
        listTugas.add(tugas);
    }

    public void removeTugas(Tugas tugas) {
        listTugas.remove(tugas);
    }

    public void incCounter(){
        this.counter++;
    }

    public void resetCounter() {
        this.counter = 0;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new HashSet<GrantedAuthority>();
    }

}
