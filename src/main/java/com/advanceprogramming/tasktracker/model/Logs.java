package com.advanceprogramming.tasktracker.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Set;

@Entity
@Table(name = "logs")
@NoArgsConstructor
@Data
public class Logs{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "pesan")
    private String pesan;

    @Column(name = "time", updatable = false, nullable = false)
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime time;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="npm")
    private Mahasiswa mahasiswa;

    public Logs(String pesan, Mahasiswa mahasiswa) {
        this.pesan = pesan;
        this.time = LocalDateTime.now();
        this.mahasiswa = mahasiswa;
    }

    public String getPesan() {
        return pesan;
    }

    public String getTimeFormatted() {
        String DATE_FORMATTER= "yyyy-MM-dd HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDateTime = this.time.format(formatter);

        return formatDateTime;
    }

}
