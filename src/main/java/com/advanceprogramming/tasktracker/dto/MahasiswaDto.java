package com.advanceprogramming.tasktracker.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MahasiswaDto {
    private String NPM;
    private String username;
    private String password;
    private String nama;

    public MahasiswaDto(String NPM, String username, String nama, String password) {
        this.NPM = NPM;
        this.username = username;
        this.password = password;
        this.nama = nama;
    }

    public String getNPM() {
        return NPM;
    }

    public void setNPM(String NPM) {
        this.NPM = NPM;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}

