Master
[![pipeline status](https://gitlab.com/rasyidmi/tk-adpro/badges/master/pipeline.svg)](https://gitlab.com/rasyidmi/tk-adpro/-/commits/master)
[![coverage report](https://gitlab.com/rasyidmi/tk-adpro/badges/master/coverage.svg)](https://gitlab.com/rasyidmi/tk-adpro/-/commits/master)
Rasyid
[![pipeline status](https://gitlab.com/rasyidmi/tk-adpro/badges/rasyid/pipeline.svg)](https://gitlab.com/rasyidmi/tk-adpro/-/commits/rasyid)
[![coverage report](https://gitlab.com/rasyidmi/tk-adpro/badges/rasyid/coverage.svg)](https://gitlab.com/rasyidmi/tk-adpro/-/commits/rasyid)
Fausta
[![pipeline status](https://gitlab.com/rasyidmi/tk-adpro/badges/fausta/pipeline.svg)](https://gitlab.com/rasyidmi/tk-adpro/-/commits/fausta)
[![coverage report](https://gitlab.com/rasyidmi/tk-adpro/badges/fausta/coverage.svg)](https://gitlab.com/rasyidmi/tk-adpro/-/commits/fausta)


**DESKRISPI**

TaskTracker merupakan sebuah aplikasi yang mampu menampilakn deadline-deadline tugas dari mata kuliah yang mahasiswa ambil (subscribe). 
Aplikasi ini masih menggunakan sistem manual untuk menambahkan tugas-tugas yang tersedia melalui admin aplikasi. Profiling aplikasi ini 
menggunakan JProfiler dan implementasi microservice menggunakan Eureka.

**Link Microservice**
SERVICE-REGISTRY https://gitlab.com/rasyidmi/task-tracker-service-registry 
API-GATEWAY https://gitlab.com/rasyidmi/task-tracker-api-gateway


**ANGGOTA**
Rasyid Miftahul Ihsan - 1906293303
Raden Fausta Anugrah Dianparama - 1906285560
